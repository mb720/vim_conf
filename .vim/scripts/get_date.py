#!/usr/bin/python

import argparse
import datetime

__author__ = "Matthias Braun"

# Gets the days till the next occurrence of a weekday. For example, if today is Monday (todays_weekday_nr is zero)
# and I call this with next_weekday_nr of 2 (i.e., Wednesday), I'll get a timedelta of two days.
def get_days_till_next_weekday(next_weekday_nr, todays_weekday_nr):
    day_difference = ((7 + next_weekday_nr) - todays_weekday_nr) % 7
    # The next occurrence of the current weekday is in seven days
    if day_difference == 0:
        day_difference = 7
    time_delta = datetime.timedelta(days = day_difference)
    return time_delta

def get_desired_date(dateString):
    today = datetime.datetime.now()
    # Create a map from date strings to the date. Then return the value associated with the parameter or a default value
    return {
            "today": today,
            "tomorrow": today + datetime.timedelta(days = 1),
            "next_monday": today + get_days_till_next_weekday(0, today.weekday()),
            "next_tuesday": today + get_days_till_next_weekday(1, today.weekday()),
            "next_wednesday": today + get_days_till_next_weekday(2, today.weekday()),
            "next_thursday": today + get_days_till_next_weekday(3, today.weekday()),
            "next_friday": today + get_days_till_next_weekday(4, today.weekday()),
            "next_saturday": today + get_days_till_next_weekday(5, today.weekday()),
            "next_sunday": today + get_days_till_next_weekday(6, today.weekday()),
            }.get(dateString, today)

parser = argparse.ArgumentParser(
    description = "Gets the string representing a date and time like today or tomorrow",
    # Shows the default value for arguments when calling --help
    formatter_class = argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument("-f", "--format",
    help = "strftime format used to display the time. See strftime.org",
    # For formatting directives see: https://docs.python.org/2/library/datetime.html#strftime-strptime-behavior
    default = "%Y-%m-%d %H:%M",
    required = False)

parser.add_argument("-d", "--date",
        help = "Specifies which date to get. Possible options: 'today', 'tomorrow', 'next_monday', 'next_tuesday', etc.",
    default = "today",
    required = False)

args = parser.parse_args()

desiredDate = get_desired_date(args.date)
formattedTime = desiredDate.strftime(args.format)

print(formattedTime)

