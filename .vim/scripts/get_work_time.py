#!/usr/bin/python

import argparse
from datetime import timedelta, datetime
import re

__author__ = 'Matthias Braun'

parser = argparse.ArgumentParser(
    description='Shows the amount of time worked, parsed from a file. We assume start and end of a work interval are written as, for example, "9:49 - 10:03"',
    # Shows the default value for arguments when calling --help
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

parser.add_argument('-f', '--file',
    help = 'The file containing the work time',
    required = True)

def without_seconds(time_diff):
  return ':'.join(str(time_diff).split(':')[:2])

args = parser.parse_args()

with open(args.file) as f:
  content = f.readlines()

# Matches, for example, "9:49 - 10:03"
time_span_regex = '(\d+:\d+)\s*-\s*(\d+:\d+)'
hourMinFormat = '%H:%M'

# The time worked
total_time = timedelta(0)

regex_obj = re.compile(time_span_regex)
for line in content:
  match = regex_obj.search(line)
  if(match):
    start_time = datetime.strptime(match.group(1), hourMinFormat)
    end_time= datetime.strptime(match.group(2), hourMinFormat)
    time_diff = end_time - start_time

    print (line.strip())
    time_diff_without_seconds = without_seconds(time_diff)
    print (time_diff_without_seconds + '\n')

    total_time += time_diff

print ("Total time %s" % without_seconds(total_time))

