# My Vim configuration

Plugins are managed by Vundle, which is placed as a submodule in `.vim/bundle/Vundle.vim`.

Clone the repository and Vundle (shallowly) like this:

    `git clone --recursive --shallow-submodules git@bitbucket.org:mb720/vim_conf`

To get the plugins via Vundle, do `:PluginInstall` inside Vim.

Make softlinks of .vimrc and ./vim to the home directory.
Hardlinks won't work since Git breaks them: https://stackoverflow.com/questions/3729278/git-and-hard-links#3731139
Also, don't forget to create links via absolute paths, not relative. Otherwise the link will open to try to open a file relative to its location.

